package b137.tenio.s02d3;


import java.util.HashMap;

public class HashMaps {
    public static void main(String[] args){
        System.out.println("HashMaps");

        HashMap<String, String> boss = new HashMap<String, String>();

        // add new elements
        boss.put("Sif", "Darkroot Basin");

        System.out.println(boss);
        boss.put("Artorias", "Oolacile Arena");

        // retrieve item via index
        System.out.println(boss.get("Artorias"));

        // get list of keys
        System.out.println(boss.keySet());

        //get list of values
        System.out.println(boss.values());

        //removing an existing item
        boss.remove("Sif");
        System.out.println(boss);


    }
}
